import java.util.Random;

public class Deck{
	//attributes
	private int numberOfCards; 
	private Card[] cards;
	Random rng;
	private String[] suits = new String[] {"HEARTS", "CLUBS", "SPADES", "DIAMONDS"};
	private int[] ranks = new int[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13};
	
	//constructor
	public Deck(){
		rng = new Random();
		this.cards = new Card[52]; //initial size of a deck
		
		//create card object in cards[]
		int x = 0;
		for (int i : ranks){ 
			for (String s : suits){
				cards[x] = new Card(i, s); //create a card
				x++;	
			}
		}
		numberOfCards = this.cards.length-1; //assign last position of the deck to numberOfCards
	}
	/*method returns how many cards are on the deck*/
	public int length(){
		return this.numberOfCards;
	}
	/*method returns the Card in the last position of the Card[]
	* reduce the numberOfCards by 1
	*/
	public Card drawTopCard(){
		Card toReturn = null;
		if (numberOfCards >= 0){
			toReturn = this.cards[numberOfCards];
			numberOfCards--;
		}
		return toReturn;
	}
	/*method returns a String containing all the Cards on the deck*/
	public String toString(){
		String str = "";
		//loop adds each card's value to a String variable 
		for (int i=0; i<numberOfCards; i++)
			str += cards[i].toString() + "\n";
		return str;
	}
	/*method shuffles the cards on the deck*/
	public void shuffle(){
		boolean[] isSwapped = new boolean[numberOfCards]; //boolean array that checks if a card is already swapped
		int swapIndex; //random position that current position will be swaped with
		Card temp; //temporary var to hold one of card while swapping 2 cards
		
		//loop run through the deck to swap cards randomly 
		//if two cards swap together --> assign true value to both position
		//end loop either when every position is true or when we reached to the end of the length
		for (int i=0; i<numberOfCards; i++){
			if (!isSwapped[i]){
				swapIndex = rng.nextInt(numberOfCards);
				
				//checks if the random position is somewhere between current position and the last position
				if (swapIndex > i){
					temp = cards[i];
					cards[i] = cards[swapIndex];
					cards[swapIndex] = temp;
					isSwapped[i] = true; //current position swapped --> true
					isSwapped[swapIndex] = true; //random position swapped --> true
				}
				//otherwise adds current position by 1 and continue checking towards the end of deck to find an unswapped position
				else {
					int x = i+1;
					//loop only run when current position has not swapped yet and the x position is still in the bound
					while (!isSwapped[i] && x<numberOfCards){
						//only swap values if the x position has not swapped, otherwise increment x then re-check 
						if (!isSwapped[x]){
							temp = cards[i];
							cards[i] = cards[x];
							cards[x] = temp;
							isSwapped[i] = true; //current position swapped --> true
							isSwapped[x] = true; //random position swapped --> true
						}
						x++;
					}
				}
			}	
		}
	}
}