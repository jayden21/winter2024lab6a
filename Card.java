public class Card{
	//attributes
					  //Ace, ...      J   Q   K
	private int rank; //1, 2, 3, ..., 11, 12, 13
	private String suit; //Hearts, Clubs, Spades, Diamonds
	
	//Constructor
	public Card(int rank, String suit){
		this.rank = rank;
		this.suit = suit;
	}
	
	//getter
	public int getRank(){
		return this.rank;
	}
	public String getSuit(){
		return this.suit;
	}
	


	public String toString(){
		String printRank = Integer.toString(this.rank);
		if (this.rank == 1)
			printRank = "ACE";
		if (this.rank == 11)
			printRank = "JACK";
		if (this.rank == 12)
			printRank = "QUEEN";
		if (this.rank == 13)
			printRank = "KING";
		return printRank + " of " + this.suit;
	}
}