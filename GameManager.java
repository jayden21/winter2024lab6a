public class GameManager{
	//attributes
	private Deck drawPile;
	private Card centerCard;
	private Card playerCard;
	
	//constructor
	public GameManager(){
		//create new Deck object + assign it to drawPile
		Deck d = new Deck();
		drawPile = d;
		//shuffle deck + initialize centerCard, playerCard by drawing 2 top cards from drawPile
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();
	}
	/*toString() returns a String 
	* representing the centerCard, playerCard
	*/
	public String toString(){
		return "Center card: " + centerCard + "\n" + "Player card: " + playerCard;	
	}
	/*method shuffles the drawPile
	* draws 2 cards from deck 
	* updates centerCard, playerCard to the 2 new cards
	*/
	public void dealCards(){
		drawPile.shuffle();
		centerCard = drawPile.drawTopCard();
		playerCard = drawPile.drawTopCard();
	}
	/*method returns an int representing
	* the number of cards on the drawPile
	*/
	public int getNumberOfCards(){
		return drawPile.length();
	}
	/*method compares centerCard and playerCard
	* calculate points: 4pts if the rank of 2 cards are equal
	* 					2pts if the suit of 2 cards are equal
	* 				   -1pts if neither is equal
	*/
	public int calculatePoints(){
		if (centerCard.getRank() == playerCard.getRank())
			return 4;
		else 
		if (centerCard.getSuit().equals(playerCard.getSuit()))
			return 2;
		else 
			return -1;
	}
}
