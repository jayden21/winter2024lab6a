public class LuckyCardGameApp{
	public static void main(String []args){
		GameManager manager = new GameManager();
		int totalPoints = 0;
		int round = 1;
		//Welcome user
		System.out.println("Welcome to Lucky Card Game!");
		//Start game loop
		while (manager.getNumberOfCards() > 1 && totalPoints < 5){
			System.out.println("Round: " + round + "\n" + "-----------------------");
			//First, print manager object
			System.out.println(manager.toString() + "\n" + "-----------------------");
			//Second, update totalPoints by calling calculatePoints() then print updated value
			totalPoints += manager.calculatePoints();
			System.out.println("Your points after round " + round + ": " + totalPoints);
			//Finally, deal new cards to the user
			manager.dealCards();
			System.out.println("**********************************");
			round++;
		}
		//Print final score and result of the game
		if (totalPoints < 5)
			System.out.println("Player loses with: " + totalPoints);
		else 
			System.out.println("Player wins with: " + totalPoints);
	}
}